

# Usage

Do **NOT** clone this repository, use the following commands.

To build a firmware using this repo:
```bash
# Create a workir, name doesn't mater
mkdir beaglebone-ai64 ; cd beaglebone-ai64

# initialize repo, do change the _repo.conf_ file path to build for a different platform
repo init -u https://gitlab.cern.ch/mro/controls/bsp/yocto-cern \
    -m build-beaglebone-ai64/repo.conf

# Synchronize repos
repo sync

# Enter build environment (docker using x-builder)
./init-env

# Build your image
bitbake core-image-minimal
```