#!/bin/bash

SCRIPTPATH=$(cd $(dirname "$0"); pwd)
PATCHDIR=${SCRIPTPATH}/patchs

die() {
    echo "$1" >&2
    exit 1
}

usage() {
    echo "Usage: auto-patch [--revert]"
}

if [ "$1" == "-h" -o "$1" == "--help" ]; then
    usage
    exit 0
elif [ "$1" == "-r" -o "$1" == "--revert" ]; then
    shift

    PATCHS=$(find "$PATCHDIR" -name "*.patch" -printf '%P\n' | sort -r)

    echo "Reverting patchs..."
    for p in ${PATCHS}; do
        [ ! -e "${PATCHDIR}/${p}.applied" ] && continue
        echo "Reverting patch: ${p}"
        pdir=$(dirname "$p")

        patch -N -R -d "$pdir" -p1 -i "${PATCHDIR}/${p}" --dry-run || die "failed to reverse patch: $p"
        patch -N -R -d "$pdir" -p1 -i "${PATCHDIR}/${p}" || die "failed to reverse patch: $p"
        rm "${PATCHDIR}/${p}.applied"
    done
else
    PATCHS=$(find "$PATCHDIR" -name "*.patch" -printf '%P\n' | sort)

    echo "Applying patchs..."
    for p in ${PATCHS}; do
        [ -e "${PATCHDIR}/${p}.applied" ] && continue
        echo "Applying patch: ${p}"
        pdir=$(dirname "$p")

        patch -N -d "$pdir" -p1 -i "${PATCHDIR}/${p}" --dry-run || die "failed to apply patch: $p"
        patch -N -d "$pdir" -p1 -i "${PATCHDIR}/${p}" || die "failed to apply patch: $p"
        touch "${PATCHDIR}/${p}.applied"
    done
fi
